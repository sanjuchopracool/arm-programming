#include <Uart1.h>
static unsigned char TX_BUFF[TX_BUFF_SIZE];
static volatile uint16_t TX_TAIL ;
static volatile uint16_t TX_HEAD ;

static unsigned char RX_BUFF[RX_BUFF_SIZE];
static volatile uint16_t RX_TAIL ;
static volatile uint16_t RX_HEAD ;

void __attribute__ ((interrupt("IRQ"))) uart1ISRHandler(void);
/* UART1 interrupt handler(ISR) */

void insertData()
{
	if(byteToSend())
	{
		TX_TAIL = (TX_TAIL + 1) & TX_BUFF_MASK;
		U1THR = TX_BUFF[TX_TAIL];
	}
}
void uart1ISRHandler()
{
	int16_t iIRvalue = U1IIR;
	int16_t lineStatus;
	iIRvalue >>= 1;   //skip checking last bit wether interrupt is pending or not
	iIRvalue &= 0x07;   // check only for last 3 bits to check interrupt;
	uint16_t tempHead;
	uint16_t tempTail;
	//print("Interrupt: %d\n", iIRvalue);
	switch(iIRvalue) 
	{
		case IIR_RLS:
		lineStatus = U1LSR; // read line status check for errors 
		if ( lineStatus & (LSR_OE|LSR_PE|LSR_FE|LSR_RXFE|LSR_BI) )
		{
			/* There are errors or break interrupt */
			/* Read LSR will clear the interrupt */
// 			print("error :)");
			tempTail = U0RBR;		/* Dummy read on RX to clear 
						interrupt, then bail out */
			break;
		}
		else if(lineStatus & LSR_THRE)
		{
			insertData();
// 			print("Error TRX\n");
			break;
		}
		else if(!(lineStatus & LSR_RDR))
		{
// 			print("Error not RBR\n");
			break;
		}
		
		case IIR_RDA:     // receieve data available
			tempHead = (RX_HEAD + 1) & RX_BUFF_MASK ;
			//print("HEAD %d, TAIL %d TEMPHead %d\n", RX_HEAD, RX_TAIL, tempHead);			
			if(tempHead == RX_TAIL)   // if(buffer is wait for data to write); // check for buffer overflow
			{
// 				print("Buffer overFlow for UART1 Rreceive buffer");
				tempHead = U1RBR; // read dummy
				break;
			}
			RX_BUFF[tempHead] = U1RBR;
			//print("[%c]\n",RX_BUFF[tempHead]);
			RX_HEAD = tempHead;   // set circular buffer new head
			break;
		case IIR_THRE:
			lineStatus = U1LSR; // read line status check for errors 
			if(lineStatus & LSR_THRE)
			insertData();
			break;
		case IIR_CTI:
// 			print("\nCha Time\n");
			break;
		default:
			break;
	}
	//print("data Available : %d \n", byteAvailableToRead());
	VICVectAddr = 0;       // Acknowledge Interrupt
}

void uart1Init(uint32_t baudRate)
{
	uint32_t Fdiv;
	Fdiv = 30000000;
    PINSEL0 |= 0x00050005;       /* Enable RxD1 and TxD1, RxD0 and TxD0 */

    U1LCR = 0x83;               /* 8 bits, no Parity, 1 Stop bit    */
    Fdiv *= 10;   // if value is 48.89 then 1 will be left 
    Fdiv /= 16; // divide by prescaler
    Fdiv /= baudRate;
	uint8_t decimal = Fdiv % 10;
	if(decimal > 5)
		decimal = 1;
	else
		decimal = 0;
	Fdiv /= 10;
    U1DLM = Fdiv / 256;							
    U1DLL = Fdiv % 256 + decimal;	
    U1LCR = 0x03;               /* DLAB = 0                         */
    U1FCR = 0x07;		/* Enable and reset TX and RX FIFO. */
    
    // now install the interrupt at priority 1
    installInterrupt(1, UART1_INT, uart1ISRHandler);
    U1IER = IER_RBR | IER_THRE | IER_RLS;	/* Enable UART0 interrupt */
    
    // initialise tx, rx head and tail to zero
     TX_HEAD = 0;
     TX_TAIL = 0;
	 RX_HEAD = 0;
	 RX_TAIL = 0;
}

uint16_t byteAvailableToRead()
{
	if(RX_HEAD >= RX_TAIL)
		return (RX_HEAD - RX_TAIL);
	else
		return (RX_BUFF_SIZE - RX_TAIL + RX_HEAD);
}
uint16_t byteToSend()
{
	if(TX_HEAD >= TX_TAIL)
		return (TX_HEAD - TX_TAIL);
	else
		return (TX_BUFF_SIZE - TX_TAIL + TX_HEAD);	
}

void readBuffer(unsigned char* buff, uint16_t length)
{
	int i = 0;
	for( ; i < length ; i++)
	{
		buff[i] = RX_BUFF[++RX_TAIL];
		RX_TAIL &= RX_BUFF_MASK;
	}
}

void writeToBuffer(unsigned char* buff, uint16_t length)
{
	uint16_t i = 0;
	uint16_t tempHead ;
	for(; i < length ; i++)
	{   tempHead = (TX_HEAD + 1) & TX_BUFF_MASK ;		
		if(tempHead == TX_TAIL)   // if(buffer is wait for data to write); // check for buffer overflow
		{
// 			print("Buffer overFlow for UART1 Transmitt buffer");
			return;
		}
		TX_BUFF[tempHead] = buff[i];
		//print("[%c]\n",RX_BUFF[tempHead]);
		TX_HEAD = tempHead;   // set circular buffer new head
	}
	TX_TAIL = (TX_TAIL + 1) & TX_BUFF_MASK;
	U1THR = TX_BUFF[TX_TAIL];
}

void writeByte(int data)
{
    while (!(U1LSR & 0x20));
    U1THR = data;
}
