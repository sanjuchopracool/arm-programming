/*
 * main.c
 *
 *  Created on: Jul 8, 2012
 *      Author: sanju
 */
#include <lpc214x.h>
#include <timer0.h>
#include <Asm.h>
#include <lowlevel.h>
#include <type.h>
#include <Serial.h>
#include <TWI_Master.h>
#include <Uart1.h>
#include <PWM.h>

#define GYRO 0xD0
#define GYRO_WHO_AM_I 0x6B
#define GYRO_CTRL    0x20;
void wait(unsigned long val)
{
    timeVal = 0;
    while(timeVal < val);
}
int main()
{
    lowLevelInit();
    IODIR0 =(1<<21);
    initSerial();
    enableIRQ();
    init_timer();
    TWI_Init_Master();
    unsigned char whoIAm = 0x11;
    print( "Who i am %X", (int)whoIAm);
    int val;
    val = 25;
    while(1)
    {
		wait(500);
        whoIAm = I2CRead(GYRO, GYRO_WHO_AM_I);
        print( "Who i am %X", (int)whoIAm);
    }
    return 0;
}

