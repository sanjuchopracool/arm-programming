#include <Serial.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

char charFromHex(unsigned char num)
{
	char ch = '0';
	if(num < 10)
		return ch + num ;
	switch(num)
	{
		case 10:
			return 'A';
		case 11:
			return 'B';
		case 12:
			return 'C';
		case 13:
			return 'D';
		case 14:
			return 'E';
		case 15:
			return 'F';
		default:
			return 0;
	}
}

void initSerial (void) // initialize at 38400
{               /* Initialize Serial Interface       */
    PINSEL0 |= 0x00050005;                  /* Enable RxD0 and TxD0              */
    U0LCR = 0x83;                          /* 8 bits, no Parity, 1 Stop bit     */
    U0DLL = 0x00000031;                            /* 16.27604167  115200 Baud Rate @ 30MHz VPB Clock  */
    //U0FDR= 0x000000E5;                     /* mul 4 and div 9*/
    U0LCR = 0x03;                          /* DLAB = 0                          */
}


/* implementation of putchar (also used by printf function to output data)    */
int sendchar (int ch)
{ 
//     if (ch == '\n')  {
//         while (!(U0LSR & 0x20));
//         U0THR = CR;                          /* output CR */
//     }
    
    while (!(U0LSR & 0x20));
    return (U0THR = ch);
}

int getchararacter (void)
{
    /* Read character from Serial Port   */
    while (!(U0LSR & 0x01));
    return (U0RBR);
}

void sendString(const char *msg)
{
    while(*msg!='\0')
    {
        sendchar(*msg);
        msg++;
    }
}

void sendBuffer(const char *msg,uint16_t num)
{
	int i = 0;
	while(i < num)
		sendchar(msg[i++]);
		
}
void print(char* format,...)
{
	va_list ap;	/* points to each unnamed arg in turn */
	char *p,*sval;
	int ival;
	double dval;

	va_start(ap,format);	/* make ap point to 1st unnamed arg */
	
	for(p=format;*p;p++)
	{
		if(*p != '%')
		{
			sendchar(*p);
			continue;
		}

		switch(*++p)
		{
			case 'd':
				ival = va_arg(ap,int);
				printInt(ival, 10);
				break;
			case 'X':
			case 'x':
				ival = va_arg(ap,int);
				printInt(ival, 16);
				break;
			case 'f':
				dval = va_arg(ap,double);
				printFloat(dval);
				break;
			case 's':
				for(sval = va_arg(ap,char *); *sval; sval++)
				sendchar(*sval);
				break;
			case 'c':
				ival = va_arg(ap, int);
				sendchar(ival);
				break;
			default:
				sendchar(*p);
				break;
		}
	}
	va_end(ap);	/* clean up when done */
}


void printInt(int n, unsigned char base)
{
	
    if( n < 0)
    {
        sendchar('-');
        n = -n;
    }
    
	switch(base)
	{
		case 16:
			sendString("0x");
			break;
		default:
			break;
	}
	
	if(n == 0)
		sendchar('0');
	
    char buffer[11] = {0};
    char i = 9;
    while(n)
    {
        buffer[i--] = charFromHex(n % base);
        n /= base;
    }
    sendString(&buffer[i + 1]);
}

void printFloat(double n)
{
	int ival = (int) n;
	printInt(ival, 10);
	/*
	 * I will print upto 5 character in double
	 */
	
	int dval = (n - ival)*100000;
	if(dval < 0)
		dval *= -1;

    sendchar('.');
	printInt(dval, 10);
}
