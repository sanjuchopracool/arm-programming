#include <timer0.h>
/* Timer0 Compare-Match Interrupt Handler (ISR) */

void tc0_cmp(void)
{
    timeVal++;
    T0IR = TxIR_MR0_FLAG; // Clear interrupt flag by writing 1 to Bit 0
    VICVectAddr = 0;       // Acknowledge Interrupt (rough?)
}

void init_timer (void)
{
    timeVal = 0;
    T0MR0 = ((FOSC*PLL_M)/(2000)) - 1;     // Compare-hit at 10mSec (-1 reset "tick")
    T0MCR = TxMCR_INT_ON_MR0 | TxMCR_RESET_ON_MR0; 	// Interrupt and Reset on MR0
    T0TCR = TxTCR_COUNTER_ENABLE;            // Timer0 Enable
    installInterrupt(2,TIMER0_INT,tc0_cmp);
}
