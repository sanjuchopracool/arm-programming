#include  <TWI_Master.h>
#include  <irq.h>
#include  <lpc214x.h>
void __attribute__ ((interrupt("IRQ"))) i2cHandler(void);
/* I2C interrupt handler(ISR) */
void i2cHandler(void) 
{
    switch(I2C0STAT)
    {
    case TWI_START:                    ///if the condition is start condition
    case TWI_REP_START:                ///if the condition is repeated start condition
        bufptr =0 ;
		I2C0CONCLR = 0x20;
    case TWI_MT_SLA_ACK:
    case TWI_MT_DATA_ACK:
        if(TWI_Mode ==TWI_READ && bufptr==2)   ///if mode is read and bufptr is 2
        {
            TWI_buff[0] +=1 ;                 ///add one to slave address byte to change data register
			I2C0CONSET = 0x20;   ///iniate a start condition   ///initiate a start condition
        }
        else if(bufptr < (TWI_numbytes +2 ))
            I2C0DAT =TWI_buff[bufptr++]; //load byte from buffer and increase the buff pointer
        else
        {
            lastTransOk=TRUE; ///last transaction is ok
            transferState = TRANSFER_DONE;
			I2C0CONSET = 0x10;	
        }
        break;

    case TWI_MR_DATA_ACK: //data have been received store the data in buffer and increase buf pointer
        TWI_buff[bufptr++]= I2C0DAT;
		I2C0CONCLR = 0x00000004;
        break;
		
    case TWI_MR_SLA_ACK: 
		break;
		
	case TWI_MR_DATA_NACK:
		if(bufptr == TWI_numbytes )  
        {
			TWI_buff[bufptr++]= I2C0DAT;
            lastTransOk = TRUE; ///last transaction is ok
            transferState = TRANSFER_DONE;
            I2C0CONSET = 0x10;
        }
      else
		  I2C0CONSET = 0x00000004;
		  break;
    default:
        lastTransOk= FALSE; ///last transaction is wrong
        transferState = TRANSFER_DONE;
        I2C0CONSET = 0x10;		
        break;
    }
    
	I2C0CONCLR = 0x08;     // clear the SI FLAG
    VICVectAddr = 0;       // Acknowledge Interrupt 
}

/****************************************************************************
Call this function to set up the TWI master to its initial standby state.
****************************************************************************/
void TWI_Init_Master() // Function to initialize master
{
    PINSEL0 |= 0x00000050;       // sel pin0.2 Pin0.3 as I2C PIN
    I2C0SCLH = 150;              // at 30,000,000 Hz for 100KHz (low + high) = 300;
    I2C0SCLL = 150;
    installInterrupt(0, I2C0_INT, i2cHandler);
}

/**********************************************************************************
call this function to check weather the trasnreceiver is busy or not
***********************************************************************************/

unsigned char Is_TWI_Busy()
{
    return transferState;
}


/***********************************************************************************
call this function to send data to slave specifying the slave address,
internal address , pointer to data to send and no of bytes to send
************************************************************************************/
void TWI_Send_Data(unsigned char slaveAddress ,
                   unsigned char internalAddress ,
                   unsigned char *data,
                   unsigned char numbytes)
{
    while(Is_TWI_Busy());
    TWI_Mode =TWI_WRITE ; ///specify the mode
    unsigned char i ;
    TWI_numbytes =numbytes; ///initialise no of bytes for interrupt
    TWI_buff[0]= slaveAddress;   ///copy slave address to 1st byte in buffer
    TWI_buff[1]= internalAddress ; ///copy internal address at the 2nd location
    for(i=0;i<numbytes;i++)
        TWI_buff[i+2] = data[i] ;
    transferState = TRANSFER_BUSY;
    lastTransOk = FALSE ;
    I2C0CONCLR = 0x000000ff;
    I2C0CONSET = 0x40;           // enable TWI
    I2C0CONSET = 0x20; ///iniate a start condition
}

/**********************************************************************************
call this function to receive data from twi specify slave address , internal address
no of bytes ,dat will be stored in buffer use fetch bytes to get the data from buffer
************************************************************************************/
void TWI_Receive_Data(unsigned char slaveAddress ,
                      unsigned char internalAddress ,
                      unsigned char numbytes)
{
    while(Is_TWI_Busy());
    TWI_Mode =TWI_READ ; ///specify the mode
    TWI_numbytes = numbytes; ///initialise no of bytes for interrupt
    TWI_buff[0]= slaveAddress;   ///copy slave address to 1st byte in buffer
    TWI_buff[1]= internalAddress ; ///copy internal address at the 2nd location
    transferState = TRANSFER_BUSY;
    lastTransOk = FALSE ;
    I2C0CONCLR = 0x000000ff;
    I2C0CONSET = 0x40;           // enable TWI
    I2C0CONSET = 0x20; ///iniate a start condition
}
/**************************************************************************************
call this function to fetch bytes from buffer
***************************************************************************************/
unsigned char TWI_Fetch_Bytes(unsigned char *data,unsigned char numbyte)
{
    unsigned char i;
    while(Is_TWI_Busy()); //wait untill the last transmission complete
    if(lastTransOk == TRUE)
    {
        for(i = 0; i < numbyte; i++)
            data[i] = TWI_buff[ 1 + i] ;
    }
    return lastTransOk;
}
/***************************************************************************************
interrupt implementation for tIs_TWI_Busywi
******************************************************************************************/

unsigned char I2CRead(unsigned char slaveAddress , unsigned char internalAddress)
{
    TWI_Receive_Data( slaveAddress , internalAddress , 1);
    unsigned char data;
    TWI_Fetch_Bytes(&data , 1);
    return data;
}
void I2CWrite(unsigned char slaveAddress , unsigned char internalAddress , unsigned char data)
{
    TWI_Send_Data(slaveAddress,internalAddress,&data,1);
}
