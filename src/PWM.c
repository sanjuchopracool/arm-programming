#include <PWM.h>
#include <lpc214x.h>

void initPWM()
{
    PINSEL0 |= 0x00008000;    //P0.7 (PWM2) as output
    PINSEL1 |= 0x00000400;    //P0.21 (PWM5) as output

    PWMPR = 0x00000002;       // prescaler is 2 so that after every 3 clock cycle counter increase so at 30MHz counter will increase at  10MHz
    PWMPCR = 0x00002400;      //set single edge on all (last 8 bits) and enable PWM2 and PWM5 (bit 10 and bit 13)
    PWMMCR = 0x00000002;      //reset on PWMMR0 match
    PWMMR0 = 0x000003E8;      // 1000 so that PWM frequency is 10KHz
    PWMLER = 0x00000024;      //enable latch resister to update the frequency
}


void enablePWM()
{
    PWMTCR = 0x00000002;      //reset presacaler counter and timer counter
    PWMTCR = 0x00000009;      //enable counter and PWM
}


void setPWM2Value(int value)
{
    PWMMR2 = value;
}


void setPWM5Value(int value)
{
    PWMMR5 = value;
}

void disablePWM()
{
    PWMTCR = 0x00000002;     //reset all the counters and stop PWM
}
