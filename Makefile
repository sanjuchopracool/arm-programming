TOOLCHAIN= arm-none-eabi-
ARM_CPU=arm7tdmi
VPATH= ./src ./other ./include /usr/arm-elf/include /usr/arm-none-eabi/include
INCLUDE= ./include  
CCFLAGS= -mcpu=$(ARM_CPU) -mthumb-interwork -ffunction-sections -gdwarf-2 -Wall 
ASFLAGS= -mcpu=$(ARM_CPU) -mthumb-interwork -mfpu=softfpa -Wall


all: Serial.o TWI_Master.o irq.o lowlevel.o main.o startup.o Uart1.o timer0.o PWM.o
	$(TOOLCHAIN)gcc -nostartfiles -o check.elf -T ./other/ROM.ld Serial.o TWI_Master.o irq.o \
	lowlevel.o main.o startup.o Uart1.o timer0.o PWM.o \
	-L/usr/arm-elf/lib -lc -lm -lg 
	$(TOOLCHAIN)objcopy -O ihex check.elf check.hex 
	$(TOOLCHAIN)objcopy -O binary check.elf check.bin 



Serial.o : Serial.c Serial.h stdlib.h stdio.h 
	$(TOOLCHAIN)gcc $(CCFLAGS) -I$(INCLUDE) -c $< 


TWI_Master.o : TWI_Master.c TWI_Master.h irq.h 
	$(TOOLCHAIN)gcc $(CCFLAGS) -I$(INCLUDE) -c $< 


irq.o : irq.c irq.h lpc214x.h
	$(TOOLCHAIN)gcc $(CCFLAGS) -I$(INCLUDE) -c $< 


lowlevel.o : lowlevel.c lowlevel.h
	$(TOOLCHAIN)gcc $(CCFLAGS) -I$(INCLUDE) -c $<

timer0.o : timer0.c timer0.h
	$(TOOLCHAIN)gcc $(CCFLAGS) -I$(INCLUDE) -c $<

PWM.o : PWM.c PWM.h
	$(TOOLCHAIN)gcc $(CCFLAGS) -I$(INCLUDE) -c $<
	
main.o : main.c lpc214x.h timer0.h Asm.h lowlevel.h type.h Serial.h
	$(TOOLCHAIN)gcc $(CCFLAGS) -I$(INCLUDE) -c $< 

Uart1.o : Uart1.c lpc214x.h irq.h type.h Serial.h
	$(TOOLCHAIN)gcc $(CCFLAGS) -I$(INCLUDE) -c $< 
	
startup.o : startup.S 
	$(TOOLCHAIN)as $(ASFLAGS) -o startup.o $< 




.PHONY: clean 
clean : 
	rm -rf *.o  *.elf *.hex *.o *.bin 
