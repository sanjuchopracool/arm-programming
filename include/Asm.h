#ifndef __ASM_H_
#define __ASM_H_

/*
 * these function are defined in startup.S File in assembly
 * use them to enable and disble interrupts in your programme.
 */


extern void enableIRQ(); ///this function is defined in assembly to enable interrupts
extern void disableIRQ(); 
#endif  /*_ASM_H_*/
