#ifndef TWI_MASTER_H_INCLUDED
#define TWI_MASTER_H_INCLUDED
#define 	TWI_START   0x08
#define 	TWI_REP_START   0x10
#define 	TWI_MT_SLA_ACK   0x18
#define 	TWI_MT_SLA_NACK   0x20
#define 	TWI_MT_DATA_ACK   0x28
#define 	TWI_MT_DATA_NACK   0x30
#define 	TWI_MT_ARB_LOST   0x38
#define 	TWI_MR_SLA_ACK   0x40
#define 	TWI_MR_SLA_NACK   0x48
#define 	TWI_MR_DATA_ACK   0x50
#define 	TWI_MR_DATA_NACK   0x58


#define 	TWI_NO_INFO   0xF8
#define 	TWI_BUS_ERROR   0x00

#define 	TWI_READ   1
#define 	TWI_WRITE   0

#define TWI_BUFFER_SIZE     8                   //twi bytes for slave address abd internal address
#define TRUE                1
#define FALSE               0

#define TRANSFER_DONE		0
#define TRANSFER_BUSY		1
static volatile  unsigned char TWI_buff[TWI_BUFFER_SIZE] ;
static volatile  unsigned char TWI_numbytes ;
static volatile  char transferState = TRANSFER_DONE ;
static volatile  unsigned char lastTransOk=FALSE ;
static volatile  unsigned char TWI_Mode ; ///read or write
static volatile  unsigned char bufptr ; ///pointer used for buffer
/****************************************************************************
Call this function to set up the TWI master to its initial standby state.
****************************************************************************/
void TWI_Init_Master();// Function to initialize master
/**********************************************************************************
call this function to check weather the trasnreceiver is busy or not
***********************************************************************************/

unsigned char Is_TWI_Busy();
/***********************************************************************************
call this function to send data to slave specifying the slave address,
internal address , pointer to data to send and no of bytes to send
************************************************************************************/
void TWI_Send_Data(unsigned char slaveAddress ,
                   unsigned char internalAddress ,
                   unsigned char *data,
                   unsigned char numbytes);
/**********************************************************************************
call this function to receive data from twi specify slave address , internal address
no of bytes ,dat will be stored in buffer use fetch bytes to get the data from buffer
************************************************************************************/
void TWI_Receive_Data(unsigned char slaveAddress ,
                   unsigned char internalAddress ,
                   unsigned char numbytes);
/**************************************************************************************
call this function to fetch bytes from buffer
***************************************************************************************/
unsigned char TWI_Fetch_Bytes(unsigned char *data,unsigned char numbyte);
unsigned char I2CRead(unsigned char slaveAddress , unsigned char internalAddress);
void I2CWrite(unsigned char slaveAddress , unsigned char internalAddress , unsigned char data);

#endif // TWI_MASTER_H_INCLUDED

