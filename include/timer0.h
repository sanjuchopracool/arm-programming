/******************************************************************************/
/*  TIMER.C: Time Functions for 10Hz Clock Tick using Timer0                  */
/******************************************************************************/
/*  Inspired by a sample application from Keil Elektronik                     */
/******************************************************************************/
/*  Sample for WinARM by M.Thomas <eversmith@heizung-thomas.de>               */
/******************************************************************************/

#ifndef _TIMER0_H
#define _TIMER0_H
#include <lpc214x.h>
#include <irq.h>
#define FOSC 12000000UL
#define PLL_M 5
#define VICVectCntl0_ENABLE (1<<5)

#define VIC_Channel_Timer0  4

#define TxTCR_COUNTER_ENABLE (1<<0)
#define TxTCR_COUNTER_RESET  (1<<1)
#define TxMCR_INT_ON_MR0     (1<<0)
#define TxMCR_RESET_ON_MR0   (1<<1)
#define TxIR_MR0_FLAG        (1<<0)

volatile unsigned long timeVal;

void __attribute__ ((interrupt("IRQ"))) tc0_cmp(void);

/* Setup Timer0 Compare-Match Interrupt         */
/* no prescaler timer runs at cclk = FOSC*PLL_M /VPB*/
void init_timer (void);
#endif 
