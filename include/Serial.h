#ifndef  SERIAL_H
#define SERIAL_H
#include <lpc214x.h>
#include <type.h>
#define CR     0x0D


void initSerial (void) ;

int sendchar (int ch);

int getchararacter (void);

void sendString(const char *msg);
// this won't check for null charachter
void sendBuffer(const char *msg,uint16_t num);

void print(char* format,...);

void printInt(int n, unsigned char base);
void printFloat(double n);
#endif  /*SERIAL_H */
