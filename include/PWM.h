#ifndef PWM_H
#define PWM_H

void initPWM();
void enablePWM();
void setPWM2Value(int value);   //value must be between 0 - 1000;
void setPWM5Value(int value);   //value must be between 0 - 1000;
void disablePWM();
#endif // PWM_H
