#ifndef _UART1_H_
#define _UART1_H_
#include <lpc214x.h>
#include <type.h>
#include <Serial.h>
#include <irq.h>

#define IER_RBR		0x01
#define IER_THRE	0x02
#define IER_RLS		0x04

#define IIR_PEND	0x01
#define IIR_RLS		0x03
#define IIR_RDA		0x02
#define IIR_CTI		0x06
#define IIR_THRE	0x01

#define LSR_RDR		0x01
#define LSR_OE		0x02
#define LSR_PE		0x04
#define LSR_FE		0x08
#define LSR_BI		0x10
#define LSR_THRE	0x20
#define LSR_TEMT	0x40
#define LSR_RXFE	0x80

//  UART Transmission buffersize ;
//  make sure size is power of 2 ,otherwise  create problem in mask operation

#define   TX_BUFF_SIZE 	 	64
#define   RX_BUFF_SIZE 	 	64
//    define Buff Mask

#define   TX_BUFF_MASK  (TX_BUFF_SIZE -1)
#define   RX_BUFF_MASK  (RX_BUFF_SIZE -1)

#if(TX_BUFF_SIZE & TX_BUFF_MASK)
     #error  TX_BUFF_SIZE is not power of 2
     #endif
#if(TX_BUFF_SIZE >256)
     #error  TX_BUFF_SIZE is greater than 256
     #endif

#if(RX_BUFF_SIZE & RX_BUFF_MASK)
     #error  RX_BUFF_SIZE is not power of 2
     #endif
#if(RX_BUFF_SIZE >256)
     #error  RX_BUFF_SIZE is greater than 256
     #endif
     
void uart1Init(uint32_t baudRate);
uint16_t byteAvailableToRead();
uint16_t byteToSend();
void writeByte(int data);
void readBuffer(unsigned char* buff, uint16_t length);
void writeToBuffer(unsigned char* buff, uint16_t length);
#endif  // _UART1_H_